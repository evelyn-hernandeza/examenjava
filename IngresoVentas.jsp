<%-- 
    Document   : IngresoVentas
    Created on : 07-09-2019, 17:25:17
    Author     : Usuario
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="cl.aiep.model.Conexion"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    Conexion combo = new Conexion();
    combo.getConnection();

    ResultSet TipoVenta = combo.cargarCombo("select VenId,VenTipo from Venta");
    ResultSet NombreCliente =combo.cargarCombo("select CliRut,CliNombre from Cliente");
    ResultSet tabla     = combo.cargarCombo("select * from vw_vista_tabla");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ingreso de Ventas</title>
    <a href="Bootstrap/css/bootstrap.min.css.map"></a>
</head>
<body>
    <form action="GuardarCliente.jsp" method="POST">

        <div class="form-row">
            <div class="form-group col-md-3">
                <label for="folio">Folio</label>
                <div class="col-sm-10">
                    <input type="text" name="folio" class="form-control" id="inputEmail3">
                </div>
            </div>     

            <div class="form-group col-md-7">
            <label for="TipoVenta">Tipo de Venta</label>
                    <select name="TipoVenta" id="Venta" size="1">
                       <option value="0">Seleccione tipo de Venta</option>
                                  
                    <%          
                        while( TipoVenta.next()){
                            out.println("<option value='"+TipoVenta.getInt(1)+"'>");
                                out.println(TipoVenta.getString(2));
                            out.println("</option>");
                        } 
                    %>
                    </select>
            </div>
                   
            <div class="form-group col-md-2">
             <label for="Fecha">Fecha Venta</label>
             <input type="text" name="Fecha" class="form-control" id="inputEmail3">
            </div> 
             
            <div class="form-group col-md-6">  
            <label for="Nombre">Nombre</label>
                    <select name="Nombre" id="Analisis" size="1">
                       <option value="0">Seleccione Usuario</option>
                                  
                    <%          
                        while( Nombre.next()){
                            out.println("<option value='"+Nombre.getInt(1)+"'>");
                                out.println(Nombre.getString(2));
                            out.println("</option>");
                        } 
                    %>
                    </select>
            </div> 
                    
            <div class="form-group col-md-6 mt-4">
                    <button type="submit" name="CrearVenta">Crear Venta</button>
            </div>  
                    
                  
                    <table border="1" class="table table-hover" style="width: 100%">
                <thead>
                    <tr>
                        <th>Folio</th>
                        <th>Tipo Venta</th>
                        <th>Fecha</th>
                        <th>Nombre</th>
                    </tr>
                </thead>

                <%
                    while (tabla.next()) {
                        out.println("<tr>");
                        out.println("<td>" + tabla.getString(1) + "</td>");
                        out.println("<td>" + tabla.getString(2) + "</td> ");
                        out.println("<td>" + tabla.getString(3) + "</td>");
                        out.println("<td>" + tabla.getString(4) + "</td>");
                        out.println("</tr>");
                    }
                %>
            </table>
    </form>
</body>
</html>

 