
package Model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;


public class Conexion {
        private String server;
        private String user;
        private String base;
        private String clave;
        private int port;
        private String url;
        private Connection conexion;

        public Conexion (){

                this.clave  = "1234567890";
                this.server = "localhost";
                this.user   = ".net";
                this.port   = 1433;
                this.base   = "EAMEN1_EvelynH";

                    this.url = "jdbc:sqlserver://localhost:1433;databaseName=EXAMEN1_EvelynH;";
               
            }
 
   
    public void getConnection() throws SQLException{
        this.conexion = null; 
        
       try{
           
           Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
           
           this.conexion = (Connection) DriverManager.getConnection(  
                   this.url,  
                   this.user, 
                   this.clave 
           );
          
           System.out.println(" exito al conectarse");
       }catch(ClassNotFoundException ex){
           
            System.out.println("Error de conexion : " + ex.getMessage());
       }    
    }
     
    public ResultSet devolverDatos( String query ) throws SQLException{
    
        return this.conexion.createStatement().executeQuery( query );
        
    }
}

    

